import sys
import numpy as np
import math
from sklearn import metrics
from sklearn.calibration import CalibratedClassifierCV
from collections import defaultdict
from learning_env import LearningEnv
import matplotlib.pyplot as plt
import inequality_util as iu
sys.path.insert(0, '../shared')
import indices
sys.path.insert(0, '../../util')
import datasets.load_adult_data as lad
import datasets.load_compas_data as lcd
import output
import parallelization as para
sys.path.insert(0, '../fairness_optimization/') # the code for fair classification is in this directory
import fair_models as fm

import pickle


#sens_feature = 'race_White'
sens_feature = 'race_Caucasian'


class FairConstEnv(LearningEnv):
    def __init__(self, opt_params={}):
        #self.loss_function = "logreg" # perform the experiments with logistic regression
        #self.EPS = 1e-6
        self.model = fm.FairnessConstrainedClassifier(opt_params=opt_params)

    def load_data(self, dataset, seed=None, val_split=0.):
        self.ds_name = dataset
        if dataset == 'Adult':
            X, y, x_control, self.feature_names = lad.load_adult_data()
        elif dataset == 'Compas':
            X, y, x_control, self.feature_names = lcd.load_compas_data()
            # invert to make beneficial outcome the positive one
            y = -y
        else:
            raise ValueError('Invalid dataset name "{}"'.format(dataset))

        if seed is not None:
            np.random.seed(seed)
            order = np.random.permutation(len(X))
            X = X[order]
            y = y[order]
            x_control = {g_name: group[order] for g_name, group in x_control.items()}

        self.sensitive_attrs = list(x_control.keys())

        #y = (y + 1.) / 2. # convert to {0, 1}
        self.setup_data(X, y, x_control, val_split=val_split)

    def train_classifier(self, proxy_type, sens_feature, cov_thres, seed=None):
        
        if proxy_type is None:
            pass
        elif cov_thres is None:
            cons_params = {'cons_type': 'cov',
                    'proxy_type': proxy_type,
                    'sens_feature': sens_feature}
            self.model.add_constraint('cov_cons', cons_params)

        else:
            self.model.set_cons_strength('cov_cons', cov_thres)

        self.model.fit(self.x_train, self.y_train, self.x_control_train, seed)

    def evaluate_classifier(self, sens_feature):
        probas = self.model.predict_proba(self.x_test)
        preds = (probas > .5).astype(float)
        labels = (self.y_test + 1) / 2

        sens_groups = {sens_feature: self.x_control_test[sens_feature], \
                'non_' + sens_feature: np.logical_not(self.x_control_test[sens_feature])}
        sens_groups['All'] = np.ones(len(labels), dtype=bool)
    
        group_stats = {}
        for group_name, group in sens_groups.items():
            group = group.astype(bool)
            group_labels = labels[group]
            group_preds = preds[group]
            group_probas = probas[group]

            accuracy = metrics.accuracy_score(group_labels, group_preds)
            conf_mat = metrics.confusion_matrix(group_labels, group_preds)
            fpr = conf_mat[0][1] / np.sum(group_labels == 0.)
            fnr = conf_mat[1][0] / np.sum(group_labels == 1.)

            diffs = np.abs(group_probas - group_labels)
            fp = diffs[group_labels == 0.]
            fn = diffs[group_labels == 1.]
            fpr = fp.mean()
            fnr = fn.mean()

            error_type_diffs = {'MCR': diffs,
                    'FPR': fp,
                    'FNR': fn,
                    'TAR': group_probas}

            table_row = [accuracy, fpr, fnr]
            for error_type, error_diffs in error_type_diffs.items():
                gini_coefficient = indices.gini_coefficient(error_diffs)
                table_row.append(gini_coefficient)
            group_stats[group_name] = table_row

        return group_stats
        
    def evaluate_fairness_tradeoffs(self, sens_feature, pop_filter,
            proxy_type):

        res_dict = {}
        res_dict_mc_rates = {}
        res_dict_intersectional_contribs = {}

        preds = self.model.predict(self.x_test)
        #scores = self.model.predict_proba(self.x_test)
        labels = (self.y_test + 1) / 2

        accuracy = metrics.accuracy_score(labels, preds)
        res_dict_mc_rates["accuracy"] = accuracy

        assert pop_filter in ['all', 'cond_negative', 'cond_positive']
        if pop_filter == 'cond_negative':
            filter_mask = ~(labels.astype(bool))
            binary_benefits = True
        elif pop_filter == 'cond_positive':
            filter_mask = labels.astype(bool)
            binary_benefits = True
        else:
            filter_mask = np.ones(len(labels), dtype=bool)
            binary_benefits = False

        benefits = indices.compute_benefits(preds, labels, binary=binary_benefits)
        benefits = benefits[filter_mask]

        preds = preds[filter_mask]
        labels = labels[filter_mask]

        #print("sens feature", sens_feature)
        #sens_groups = iu.compute_sensitive_groups(
        #        [sens_feature], self.x_control_test)
        sens_group = self.x_control_test[sens_feature][filter_mask].astype(bool)
        pret_sens_feature = iu.pretty_features(sens_feature)
        sens_groups = {pret_sens_feature: sens_group,
                "non-" + pret_sens_feature: ~sens_group}
        
        decomp = iu.get_inequality_decomp(benefits, sens_groups)
        for res_key in ['overall_ineq', 'intergroup_ineq']:
            res_dict[res_key] = decomp[res_key]
        for i, g_name in enumerate(decomp['group_names']):
            res_dict[g_name] = decomp['subgroup_ineqs'][i]
        #print("intergroup_ineq:", decomp['intergroup_ineq'])
        
        sens_groups['All'] = np.ones(len(labels), dtype=bool)
        for g_name, group in sens_groups.items():
            group_labels = labels[group]
            group_preds = preds[group]
            conf_mat = metrics.confusion_matrix(group_labels, group_preds, labels=[0, 1])
            if proxy_type == 'OMR':
                accuracy = metrics.accuracy_score(group_labels, group_preds)
                res_dict_mc_rates["{} accuracy".format(g_name)] = accuracy
            elif proxy_type == 'FPR':
                fpr = conf_mat[0][1] / np.sum(group_labels == 0)
                res_dict_mc_rates["{} FPR".format(g_name)] = fpr
            elif proxy_type == 'FNR':
                fnr = conf_mat[1][0] / np.sum(group_labels == 1)
                res_dict_mc_rates["{} FNR".format(g_name)] = fnr

        feature_split_order = {'Compas': ['sex', 'race', 'age'],
                #'Adult': ['marital', 'relationship', 'workclass', 'education']}
                'Adult': ['marital', 'relationship']}#, 'workclass']}#, 'education']}
       
        # intersectional inequality decomposition
        split_features = feature_split_order[self.ds_name]
        ds_features_groups = iu.create_feature_groups(
                split_features, self.feature_names,
                self.x_test, self.x_control_test)
        ds_features_groups = {fn: fc[filter_mask] for fn, fc in ds_features_groups.items()}

        split_features = feature_split_order[self.ds_name]
        intersectional_labels, intergroup_ineq_fracs, intra_ineq_fracs = iu.compute_intersectional_inequalities(split_features,
                ds_features_groups, benefits)
        for is_label, ineq_frac in zip(intersectional_labels, intergroup_ineq_fracs):
            res_dict_intersectional_contribs[is_label] = ineq_frac

        return res_dict, res_dict_mc_rates, res_dict_intersectional_contribs

def eval_fair_const_kernel(class_env, sens_feature, pop_filter, proxy_type, cons_strength):
    class_env.train_classifier(proxy_type, sens_feature, cons_strength)
    return class_env.evaluate_fairness_tradeoffs(sens_feature, pop_filter,
            proxy_type)

def evaluate_fair_const():
    datasets = {
            #'Adult': {'sens_features': ['race_White', 'sex_Female'], 'opt_params': {'tau': 2}, 'proxy_types': ['OMR']}, #UTIL
            'Compas': {'sens_features': ['race_Caucasian'],#, 'sex'],
                #'opt_params': {'tau': 4},
                'opt_params': {'tau': 5.0, 'mu': 1.2, 'eps': 1e-4},
                'proxy_types': ['FPR', 'FNR']} #UTIL
            }

    cons_strength_steps = 10
    cons_strengths = [i / (cons_strength_steps) for \
            i in range(cons_strength_steps + 1)][::-1]
    #cons_strengths[-1] = .001
    print("cons strengths:", cons_strengths)

    output.set_paper_style()
    plt.rcParams['legend.fontsize'] = 34
    plt.rcParams['axes.labelsize'] = 38
    plt.rcParams['xtick.labelsize'] = 32
    plt.rcParams['ytick.labelsize'] = 32

    for dataset, ds_config in datasets.items():
        out_dir = 'results/fair_const/' + dataset + '/'
        output.create_dir(out_dir)

        proxy_types = ds_config['proxy_types']
        sens_features = ds_config['sens_features']

        class_env = FairConstEnv(opt_params=ds_config['opt_params'])

        for sens_feature in sens_features:
            print("Evaluating sens_feature", sens_feature)

            for proxy_type in proxy_types:
                print('Evaluating {} constraint'.format(proxy_type))

                if proxy_type == 'FPR':
                    pop_filter = 'cond_negative'
                elif proxy_type == 'FNR':
                    pop_filter = 'cond_positive'

                plot_results = defaultdict(list)
                plot_results_mc_rates = defaultdict(list)
                plot_results_intersectional_contributions = defaultdict(list)

                seeds = [829, 4902, 29401, 1804, 32011] + \
                        [3, 8022258, 5801, 32, 729182]
                for i, seed in enumerate(seeds):
                    print("Evaluating for seed", i)

                    class_env.load_data(dataset, seed=seed)
                    class_env.model.clear_constraints()
                    # train once with 0 constraint strength to initialize reference value
                    class_env.train_classifier(proxy_type, sens_feature, None, seed)

                    eval_kernel = para.wrap_function(eval_fair_const_kernel)
                    results = para.map_parallel(eval_kernel, cons_strengths,
                            invariant_data=(class_env, sens_feature, pop_filter, proxy_type))

                    # convert the list of dicts to dicts of lists (of lists, one for each seed)
                    for dict_results, target_dict in zip(para.extract_positions(results, range(3)),
                            [plot_results, plot_results_mc_rates,
                                plot_results_intersectional_contributions]):
                        for key in dict_results[0]:
                            seed_results = []
                            for res in dict_results:
                                seed_results.append(res[key])
                            target_dict[key].append(seed_results)
                
                for target_dict in [plot_results, plot_results_mc_rates,
                        plot_results_intersectional_contributions]:
                    for key in target_dict:
                        target_dict[key] = para.aggregate_results(target_dict[key], axis=0)

                # Output

                between_ineq_types = ['overall_ineq', 'intergroup_ineq']

                cov_label ="Covariance threshold"
                #ax.set_ylabel("Unfairness ($\mathcal{E}_2$)")

                color_dicts = [{None: 'tab:orange'}, {None: None}]
                for ineq_type, y_label, colors in zip(between_ineq_types, [
                    "Individual\nunfairness ($\mathcal{E}^2$)",
                    "Between-group\nunfairness ($\mathcal{E}^2_\\beta$)"],
                    color_dicts):
                    iu.plot_curves(
                            iu.FIG_TYPE_CONSTRAINT_INEQ_DECOMP,
                            "{}_{}_constraint_{}_{}".format(sens_feature,
                                proxy_type, pop_filter, ineq_type),
                            cons_strengths, cov_label,
                            {None: plot_results[ineq_type]}, y_label,
                            colors=colors)

                # TODO: hardcoded
                color_dict = {'Whites': 'g', 'non-Whites': 'm'}
                iu.plot_curves(
                        iu.FIG_TYPE_CONSTRAINT_INEQ_DECOMP,
                        "{}_{}_constraint_{}_intragroup_ineq".format(sens_feature,
                            proxy_type, pop_filter),
                        cons_strengths, cov_label,
                        {intra_ineq: plot_results[intra_ineq] for intra_ineq in plot_results if \
                                intra_ineq not in between_ineq_types},
                        "Within-group\nunfairness ($\mathcal{E}^2_\\omega$)",
                        colors=color_dict)
                iu.plot_curves(
                        iu.FIG_TYPE_CONSTRAINT_INEQ_DECOMP,
                        "{}_{}_constraint_{}_group_MCRs".format(sens_feature,
                            proxy_type, pop_filter),
                        cons_strengths, cov_label,
                        plot_results_mc_rates, proxy_type)

                iu.plot_curves(
                        iu.FIG_TYPE_CONSTRAINT_INEQ_DECOMP,
                        "{}_{}_constraint_{}_intersectional_ineq".format(sens_feature,
                            proxy_type, pop_filter),
                        cons_strengths, cov_label,
                        plot_results_intersectional_contributions,
                        proxy_type)

        iu.plot_results(out_dir, dataset)

        wiki_file_loc = iu.get_wiki_file(out_dir)
        with open(wiki_file_loc, 'a') as wiki_file:
            description = "Overall, intergroup and intragroup inequality when applying fairness constraints.\nAll/cond_positive/cond_negative denotes which part of the population is considered in the inequality analysis."
            iu.emit_curves(wiki_file, out_dir,
                    dataset,
                    iu.FIG_TYPE_CONSTRAINT_INEQ_DECOMP,
                    description)
        iu.clear_figures()

def main():
    evaluate_fair_const()

if __name__ == '__main__':
    main()
