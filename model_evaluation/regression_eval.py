import sys
import numpy as np
from sklearn import linear_model
from sklearn import metrics
from sklearn.datasets import load_boston
from sklearn.svm import SVR
from sklearn import tree
from learning_env import LearningEnv
import inequality_util
sys.path.insert(0, '../shared')
import indices
sys.path.insert(0, '../../util')
import datasets.load_adult_data as lad
import datasets.load_compas_data as lcd
import datasets.load_tic_data as ltd
import datasets.load_news_pop_data as lnpd
import datasets.load_student_performance_data as lspd
import output

REGRESSION_METHODS_INFO = {'Linear': ('Linear regression', 'http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html'),
    'Lasso': ('Lasso regression', 'http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html'),
    'Ridge': ('Ridge regression', 'http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html'),
    'SVR': ('Support vector regression', 'http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVR.html'),
    'CART': ('Decision tree regression', 'http://scikit-learn.org/stable/auto_examples/tree/plot_tree_regression.html')}

class RegressionEnv(LearningEnv):
    def __init__(self):
        self.hyperparam_store_loc = 'hyperparams/regression_params'
        output.create_dir('hyperparams')

    def load_data(self, dataset):
        self.ds_name = dataset
        if dataset == 'CensusIncome':
            X, y, x_control = lad.load_census_income_data()
        elif dataset == 'BostonHousing':
            X, y = load_boston(return_X_y=True)
            x_control = {}
        elif dataset == 'CommunitiesCrime':
            X, y, x_control = lad.load_communities_crime_data()
        elif dataset == 'Adult':
            X, y, x_control = lad.load_adult_data()
        elif dataset == 'Compas':
            X, y, x_control = lcd.load_compas_data()
            # invert y to make larger values more desirable
            y = -y
        elif dataset == 'TIC':
            X, y, x_control = ltd.load_tic_data()
        elif dataset == 'NewsPopularity':
            X, y, x_control = lnpd.load_news_pop_data()
        elif dataset == 'StudentPerformance':
            X, y, x_control = lspd.load_student_performance_data()
        else:
            raise ValueError('Invalid dataset name "{}"'.format(dataset))

        self.setup_data(X, y, x_control)

    def setup_regressor(self, regressor):
        default_penalty_range = [.01, .05, .1, .2, .5, 1., 2., 5., 10., \
                20., 50, 100.]
        if regressor == 'Linear':
            model = linear_model.LinearRegression
            hyperparam_ranges = {}
        elif regressor == 'Lasso':
            model = linear_model.Lasso
            hyperparam_ranges = {'alpha': default_penalty_range}
        elif regressor == 'Ridge':
            model = linear_model.Ridge
            hyperparam_ranges = {'alpha': default_penalty_range}
        elif regressor == 'SVR':
            model = SVR
            hyperparam_ranges = {'C': default_penalty_range}
        elif regressor == 'CART':
            model = tree.DecisionTreeRegressor
            hyperparam_ranges = {'max_depth': [i+1 for i in range(8)]}
        #Isotonic regression, ...
        else:
            raise ValueError('Invalid regressor "{}"'.format(regressor))

        super(RegressionEnv, self).setup_model(model, regressor, hyperparam_ranges)

    def evaluate_regressor(self, method, predictions, min_diff):
        #print("adding min diff", min_diff)
        #print('mean wage:', np.mean(self.y_test))
        #print('test shape:', self.x_test.shape)

        #scores = self.model.predict(self.x_test)
        scores = predictions
        pred_diffs = scores - self.y_test
        abs_diffs = np.abs(pred_diffs)
        #sqr_diffs = np.square(pred_diffs)
        # TODO: do we want to shift the minimum to zero even if it is > 0?
        #shifted_diffs = pred_diffs - min_diff if min_diff < 0 else pred_diffs
        #sigmoid_diffs = 1 / (1 + np.exp(-pred_diffs))

        #gini_ae = indices.gini_coefficient(abs_diffs)
        #gini_se = indices.gini_coefficient(sqr_diffs)

        #gini_shifted = indices.gini_coefficient(shifted_diffs)
        #gini_sigmoid = indices.gini_coefficient(sigmoid_diffs)
        #gini = indices.gini_coefficient(pred_diffs)
        #theil_shifted = indices.theil_index(shifted_diffs)
        #theil_sigmoid = indices.theil_index(sigmoid_diffs)
        #theil_neg_adj = indices.theil_index(pred_diffs,
        #        adjust_for_neg = True)
        #ge2_shifted = indices.ge_2_index(shifted_diffs)
        #ge2_sigmoid = indices.ge_2_index(sigmoid_diffs)
        #ge2 = indices.ge_2_index(pred_diffs)

        ineq_res, col_names = indices.evaluate_inequality(
                pred_diffs,
                ['Gini', 'Theil', 'GE_2'],
                ['shift', 'sigmoid', 'none'],
                min_diff)

        #inequality_util.plot_lorenz_curve(abs_diffs, method)
        inequality_util.plot_lorenz_curve(pred_diffs, method,
                fig_name="utilities")

        table_row = [metrics.mean_absolute_error(self.y_test, scores)] + ineq_res
                ##gini_ae, gini_shifted, theil_shifted, ge2]
                #gini_shifted, theil_shifted, ge2_shifted,
                #gini_sigmoid, theil_sigmoid, ge2_sigmoid,
                #gini, theil_neg_adj, ge2]
        return table_row, ["MAE"] + col_names

def evaluate_regression():
    datasets = [ \
        'BostonHousing', \
        'CensusIncome', \
        'CommunitiesCrime', \
        'Adult', \
        'Compas', \
        'TIC', \
        'NewsPopularity', \
        'StudentPerformance' \
    ]

    for dataset in datasets:
        print('Evaluating on dataset {}'.format(dataset))

        out_dir = 'results/regression/' + dataset + '/'
        output.create_dir(out_dir)
        output.set_paper_style()

        reg_env = RegressionEnv()
        reg_env.load_data(dataset)

        method_results = {}
        hyperparams = {}
        predictions = []
        min_pred_diff = float('inf')

        #['Linear', 'Lasso', 'Ridge', 'SVR', 'CART']:
        for method in REGRESSION_METHODS_INFO:
            print('\nEvaluating {}'.format(method))
            params = reg_env.train_regressor(method)
            hyperparams[method] = params

            prediction = reg_env.model.predict(reg_env.x_test)
            predictions.append(prediction)
            pred_diffs = prediction - reg_env.y_test
            min_diff = np.min(pred_diffs)
            if min_diff < min_pred_diff:
                min_pred_diff = min_diff

        col_names = None
        for pred, method in zip(predictions, REGRESSION_METHODS_INFO):
            eval_res, col_names = reg_env.evaluate_regressor(method, pred,
                    min_pred_diff)
            method_results[method] = eval_res

        inequality_util.plot_results(out_dir, dataset)
        #result_labels = ['MSE', 'Gini(SE)', 'MAE', 'Gini(AE)']
        #result_format = ['2', '2', '2', '2']
        #result_labels = ['MAE', 'Gini (AE)', 'Gini (shifted)',
        #        'Theil (shifted)', 'GE(2)']
        #result_format = ['2', '3', '3', '3', '2']
        #result_labels = ["MAE",
        #        "Gini (shifted)", "Theil (shifted)", "GE_2 (shifted)",
        #        "Gini (sigmoid)", "Theil (sigmoid)", "GE_2 (sigmoid)",
        #        "Gini", "Theil (abs mean)", "GE_2"]
        #col_format = ['2', '3', '3', '3', '3', '3', '3',
        #        '3', '3', '3']
        col_format = ['3'] * len(col_names)
        col_format[0] = '2'
        lorenz_desc = "error (y_hat - y)"
        inequality_util.write_wiki_results(out_dir, dataset,
                col_names, method_results, col_format,
                hyperparams, REGRESSION_METHODS_INFO,
                lorenz_desc)

def main():
    evaluate_regression()

if __name__ == '__main__':
    main()
