import os,sys
import numpy as np
# from generate_synthetic_data import *
sys.path.insert(0, '../../fair_classification/') # the code for fair classification is in this directory
import utils as ut
import funcs_disp_mist as fdm
import loss_funcs as lf # loss funcs that can be optimized subject to various constraints
# import plot_syn_boundaries as psb
from copy import deepcopy
import matplotlib.pyplot as plt # for plotting stuff
import pickle

sys.path.insert(0, '../../util/datasets/')
from load_compas_data import load_compas_data

#from load_compas_data import *
#from load_compas_data_till import *


SEED = 1234
#SEED = 29096
#seed(SEED)
np.random.seed(SEED)

def test_synthetic_data(seed):
        
        """ Generate the synthetic data """
        # data_type = 1
        # X, y, x_control = generate_synthetic_data(data_type=data_type, plot_data=False) # set plot_data to False to skip the data plot
        X, y, x_control, _ = load_compas_data()
        # inverting y so that positive labels are desirable outcomes
        y = -y

        ############################################################
        # Changes to Till's code to make it work
        x_control["race"] = x_control["race_Caucasian"]
        for k in list(x_control.keys()):
                if k!= "race":
                        del x_control[k]
        print(x_control)
        ############################################################
        sensitive_attrs = list(x_control.keys())

        np.random.seed(seed)
        order = np.random.permutation(len(X))
        X = X[order]
        y = y[order]
        x_control = {g_name: group[order] for g_name, group in x_control.items()}

        """ Split the data into train and test """
        train_fold_size = 0.7
        x_train, y_train, x_control_train, x_test, y_test, x_control_test = ut.split_into_train_test(X, y, x_control, train_fold_size)

        data_dict = {'x_train': x_train,
                'y_train': y_train,
                'x_control_train': x_control_train,
                'x_test': x_test,
                'y_test': y_test,
                'x_control_test': x_control_test}
        persist_dict = {'seed': seed, 'data': data_dict}

        cons_params = None # constraint parameters, will use them later
        loss_function = "logreg" # perform the experiments with logistic regression
        EPS = 1e-4

        def train_test_classifier():
                w = fdm.train_model_disp_mist(x_train, y_train, x_control_train, loss_function, EPS, cons_params)

                train_score, test_score, cov_all_train, cov_all_test, s_attr_to_fp_fn_train, s_attr_to_fp_fn_test = fdm.get_clf_stats(w, x_train, y_train, x_control_train, x_test, y_test, x_control_test, sensitive_attrs)

                
                # accuracy and FPR are for the test because we need of for plotting
                # the covariance is for train, because we need it for setting the thresholds
                return w, test_score, s_attr_to_fp_fn_test, cov_all_train
                

        """ Classify the data while optimizing for accuracy """
        print()
        print("== Unconstrained (original) classifier ==")
        w_uncons, acc_uncons, s_attr_to_fp_fn_test_uncons, cov_all_train_uncons = train_test_classifier()
        print("\n-----------------------------------------------------------------------------------\n")

        """ Now classify such that we optimize for accuracy while achieving perfect fairness """
        
        print()
        print("== Classifier with fairness constraint ==")

        it = 0.05
        mult_range = np.arange(1.0, 0.0-it, -it).tolist()
        mult_range = [round(el, 2) for el in mult_range]

        #cons_type = 1 # FPR constraint -- just change the cons_type, the rest of parameters should stay the same
        tau = 5.0
        mu = 1.2

        persist_dict['weights'] = {}

        for cons_type in [1, 2]:

            acc_arr = []
            fpr_per_group = {0:[], 1:[]}
            fnr_per_group = {0:[], 1:[]}

            threshold_weights = {}

            for m in mult_range:
                    print("factor:", m)
                    sensitive_attrs_to_cov_thresh = deepcopy(cov_all_train_uncons)
                    for s_attr in list(sensitive_attrs_to_cov_thresh.keys()):
                            for cov_type in list(sensitive_attrs_to_cov_thresh[s_attr].keys()):
                                    for s_val in sensitive_attrs_to_cov_thresh[s_attr][cov_type]:
                                            sensitive_attrs_to_cov_thresh[s_attr][cov_type][s_val] *= m


                    cons_params = {"cons_type": cons_type, 
                                                    "tau": tau, 
                                                    "mu": mu, 
                                                    "sensitive_attrs_to_cov_thresh": sensitive_attrs_to_cov_thresh}

                    w_cons, acc_cons, s_attr_to_fp_fn_test_cons, cov_all_train_cons  = train_test_classifier()
                    
                    # dist_boundary_test = np.dot(x_test, w_cons)
                    threshold_weights[m] = w_cons

                    fpr_per_group[0].append(s_attr_to_fp_fn_test_cons["race"][0.0]["fpr"])
                    fpr_per_group[1].append(s_attr_to_fp_fn_test_cons["race"][1.0]["fpr"])
                    fnr_per_group[0].append(s_attr_to_fp_fn_test_cons["race"][0.0]["fnr"])
                    fnr_per_group[1].append(s_attr_to_fp_fn_test_cons["race"][1.0]["fnr"])


                    acc_arr.append(acc_cons)

            if cons_type == 1:
                key = 'FPR'
            elif cons_type == 2:
                key = 'FNR'
            persist_dict['weights'][key] = threshold_weights

            fs = 15

            if cons_type == 1:
                ax = plt.subplot(2,1,1)
                plt.plot(mult_range, fpr_per_group[0], "-o" , color="green", label = "Group-0")
                plt.plot(mult_range, fpr_per_group[1], "-o", color="blue", label = "Group-1")
                ax.set_xlim([max(mult_range), min(mult_range) ])
                plt.ylabel('False positive rate', fontsize=fs)
                ax.legend(fontsize=fs)

            if cons_type == 2:
                ax = plt.subplot(2,1,1)
                plt.plot(mult_range, fnr_per_group[0], "-o" , color="green", label = "Group-0")
                plt.plot(mult_range, fnr_per_group[1], "-o", color="blue", label = "Group-1")
                ax.set_xlim([max(mult_range), min(mult_range) ])
                plt.ylabel('False negative rate', fontsize=fs)
                ax.legend(fontsize=fs)  

            ax = plt.subplot(2,1,2)
            plt.plot(mult_range, acc_arr, "-o" , color="green", label = "")
            ax.set_xlim([max(mult_range), min(mult_range) ])
            plt.xlabel('Covariance multiplicative factor (m)', fontsize=fs)
            plt.ylabel('Accuracy', fontsize=fs)

            plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.5)
            plt.savefig("img/fairness_acc_tradeoff_cons_type_%d.png" % cons_type)
            #plt.show()

        return persist_dict


def main():
    seeds = [829, 4902, 29401, 1804, 32011] + \
            [3, 8022258, 5801, 32, 729182]
    seed_res = []
    for i, seed in enumerate(seeds):
        print("training for seed {}".format(i))
        seed_res.append(test_synthetic_data(seed))

    with open('weights.pkl', 'wb') as weights_file:
        pickle.dump(seed_res, weights_file, pickle.HIGHEST_PROTOCOL)



if __name__ == '__main__':
        main()
