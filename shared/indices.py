import numpy as np
import math
from itertools import product

def gini_coefficient(values):
    gini_numerator = np.sum( np.sum(np.abs(values - val)) for val in values )
    gini_denominator = 2 * len(values) * np.sum(values)
    return gini_numerator / gini_denominator

def theil_index(values, adjust_for_neg=False):
    mean = np.mean(values)

    #if adjust_for_neg:
    values = [abs(v) + (abs(mean) if v * mean < 0 else 0) for v in values]
    mean = abs(mean)

    theil = sum((v / mean * np.log(v / mean) if v != 0 \
            else 0.) for v in values)
    return theil / len(values)

def ge_2_index(values):
    mean = np.mean(values)
    if mean == 0:
        # TODO
        return 0.
    ge_2 = np.sum(np.square(values / mean) - 1)
    ge_2 /= (len(values) * 2)
    return ge_2

def evaluate_inequality(diffs, use_indices, transformations, min_diff=None):
    trans_diffs = {}
    for transform in transformations:
        if transform == 'none':
            trans_diffs['raw'] = diffs
        elif transform == 'shift':
            shifted_diffs = diffs - min_diff if min_diff < 0 else diffs
            trans_diffs['shifted'] = shifted_diffs
        elif transform == 'sigmoid':
            sigmoid_diffs = 1 / (1 + np.exp(-diffs))
            trans_diffs['sigmoid'] = sigmoid_diffs
        #elif transform == 'mean-dist':
        #    mean = np.mean(values)
        #    mean_dist_diffs = [abs(v) + (abs(mean) if v * mean < 0 else 0) for v in values]
        #    mean = abs(mean)
        #    trans_diffs['mean_dist'] = mean_dist_diffs
        else:
            assert False, "Unrecognized transformation {}".format(transform)

    ineq_indices = {'Gini': gini_coefficient,
            'Theil': theil_index,
            'GE_2': ge_2_index}
    indices = {}
    for index in use_indices:
        indices[index] = ineq_indices[index]

    result = []
    col_names = []
    #for (index, index_func), (transform, diff) in product(
    #        indices.items(), trans_diffs.items()):
    for (transform, diff), (index, index_func) in product(
            trans_diffs.items(), indices.items()):
        ind_val = index_func(diff)
        result.append(index_func(diff))
        col_names.append("{} ({})".format(index, transform))

    return result, col_names

def decompose_inequality(group_benefits):
    overall_benefits = np.concatenate(group_benefits)
    overall_ineq = ge_2_index(overall_benefits)
    mean_util = np.mean(overall_benefits)

    sub_inequalities = []
    unweighted_inequalities = []
    intergroup_components = []
    intergroup_inequality = 0
    for i, sub_ut in enumerate(group_benefits):
        sub_mean_util = np.mean(sub_ut)
        means_sqr = np.square(sub_mean_util / mean_util)
        sub_inequality_weight = (len(sub_ut) / len(overall_benefits)) * means_sqr

        sub_inequality = ge_2_index(sub_ut)
        sub_inequalities.append(sub_inequality_weight * sub_inequality)
        unweighted_inequalities.append(sub_inequality)

        intergroup_component = len(sub_ut) / (2 * len(overall_benefits)) * \
                (means_sqr - 1)
        intergroup_inequality += intergroup_component
        intergroup_components.append(intergroup_component)

    #    norm = len(sub_ut)
    #    print("size", i, ":", len(sub_ut))
    #    print("0:", sum(sub_ut == 0) / norm, "1:", sum(sub_ut == 1) /norm, "2:", sum(sub_ut == 2) /norm)
    #    #print("sub", i, "ut mean:", sub_mean_util)
    #    print("sub ln:", sub_ln, ", weighted:", sub_weight * sub_ln, "\n")

    #print("intergroup inequality:", intergroup_inequality, "\n")

    decomposition_sum = sum(sub_inequalities) + intergroup_inequality
    assert math.isclose(overall_ineq, decomposition_sum, rel_tol=0.1), "overall inequality {} and decomposition sum {} not close".format(overall_ineq, decomposition_sum)

    return {'overall_ineq': overall_ineq,
            'intergroup_ineq': intergroup_inequality,
            'subgroup_ineqs': sub_inequalities,
            'unweighted_ineqs': unweighted_inequalities,
            'intergroup_components': intergroup_components}

def compute_benefits(predictions, labels, binary=False):
    if binary:
        return predictions
    else:
        return (predictions - labels) + 1
