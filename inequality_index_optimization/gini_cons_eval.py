import sys
import numpy as np
import matplotlib.pyplot as plt
import csv
import copy
from collections import defaultdict
from itertools import product, repeat
import multiprocessing as mp
sys.path.insert(0, '../../util')
import datasets.load_compas_data as lcd
import datasets.load_adult_data as lad
import output
sys.path.insert(0, '../fairness_optimization/') # the code for fair classification is in this directory
import utils as ut
import fair_models as fm
import stats
#from ..fairness_optimization import utils as ut
#from ..fairness_optimization import fair_models as fm
#from ..fairness_optimization import stats
sys.path.insert(0, '../model_evaluation/')
import fair_const_eval

def one_group_per_person(num_users):
    x_control = {}
    for i in range(num_users):
        sel = np.zeros(num_users, dtype=bool)
        sel[i] = True
        x_control[str(i)] = sel
    return x_control

class ClassifierEnv(object):
    def __init__(self, cons_params):
        opt_params = {'eps': 1e-6, 'tau': 5.0, 'mu': 1.2}
        obj_params = {'loss_func': 'logreg'}
        cons_params = cons_params
        self.model = fm.FairnessConstrainedClassifier(
                opt_params, obj_params, cons_params)

    def set_cons_params(self, cons_params):
        """
        Returns a copy of this ClassifierEnv that uses new
        cons_params
        """
        
        new_env = copy.copy(self)
        new_env.model.cons_params = cons_params
        return new_env

    def load_data(self, dataset):
        self.dataset = dataset
        if dataset == 'ProPublica': #compass
            X, y, x_control = lcd.load_compas_data()
        elif dataset == 'Adult':
            X, y, x_control = lad.load_adult_data()
        else:
            raise ValueError('Invalid dataset name {}'.format(dataset))
        
        #x_control = one_group_per_person(len(y))
        #print('new x_control:', x_control)

        self.sensitive_attrs = list(x_control.keys())
        print('sensitive attrs:', self.sensitive_attrs)

        """ Split the data into train and test """
        train_fold_size = 0.7
        self.x_train, self.y_train, self.x_control_train, self.x_test, self.y_test, self.x_control_test = ut.split_into_train_test(X, y, x_control, train_fold_size)

    def train_test_classifier(self, cons_strength):
        #w = fdm.train_model_disp_mist(self.x_train, self.y_train, self.x_control_train, self.loss_function, self.EPS, cons_params)

        self.model.set_cons_strength(cons_strength)
        self.model.fit(self.x_train, self.y_train,
                self.x_control_train)
        w = self.model.w

        #train_score, test_score, cov_all_train, cov_all_test, s_attr_to_fp_fn_train, s_attr_to_fp_fn_test = fdm.get_clf_stats(w, x_train, y_train, x_control_train, x_test, y_test, x_control_test, sensitive_attrs)
        train_stats, test_stats = stats.get_clf_stats(w, self.x_train, self.y_train, self.x_control_train, self.x_test, self.y_test, self.x_control_test, self.sensitive_attrs)
        
        # accuracy and FPR are for the test because we need of for plotting
        #return w, test_score, s_attr_to_fp_fn_test
        return w, test_stats

def train_test_unconst(clas_env):
    """ Classify the data while optimizing for accuracy """
    print("\n== Unconstrained (original) classifier ==")
    w_uncons, test_stats = clas_env.train_test_classifier(None)
    #print("\n-----------------------------------------------------------------------------------\n")
    return w_uncons, test_stats

def train_test_classic_fairness_const(clas_env):
    """ Now classify such that we optimize for accuracy while achieving perfect fairness """
    
    print("\nDisparate mistreatment version\n== Constraints on FPR ==")     # setting parameter for constraints
    cons_type = 1 # FPR constraint -- just change the cons_type, the rest of parameters should stay the same
    tau = 5.0
    mu = 1.2
    #sensitive_attrs_to_cov_thresh = {"race": {0:{0:0, 1:0}, 1:{0:0, 1:0}, 2:{0:0, 1:0}}} # zero covariance threshold, means try to get the fairest solution
    #sensitive_attrs_to_cov_thresh = {"race_African-American": {0:{0:0, 1:0}}, "race_Caucasian": {0:{0:0, 1:0}}, "race_Hispanic": {0:{0:0, 1:0}}} # zero covariance threshold, means try to get the fairest solution
    sensitive_attrs_to_cov_thresh = {}
    for race in ["race_African-American", "race_Caucasian", "race_Hispanic"]:
        sensitive_attrs_to_cov_thresh[race] = {0:{0:0, 1:0}, 1: {0:0, 1:0}, 2: {0:0, 1:0}} # zero covariance threshold, means try to get the fairest solution
    cons_params = {"cons_type": cons_type, 
                    "tau": tau, 
                    "mu": mu, 
                    "sensitive_attrs_to_cov_thresh": sensitive_attrs_to_cov_thresh}

    assert False
    w_cons, test_stats = clas_env.train_test_classifier(cons_params)
    print("\n---------------------------------------------------------\n")
    return w_cons, test_stats

def plot(res_dir, y_label, plot_name, include_legend=True):
    fig = plt.gcf()
    fig.set_size_inches(12,8)
    ax = plt.gca()
    ax.set_xlabel('Constraint threshold')
    ax.set_ylabel(y_label)
    ax.invert_xaxis()
    if include_legend:
        plt.legend()
    plt.tight_layout()
    #plt.show()
    # potentially sue plot_name.lower()
    fig_name = res_dir + '/plot_' + plot_name
    plt.savefig(fig_name + '.png')
    plt.savefig(fig_name + '.pdf')
    plt.clf()

def save_result(file_name, file_data):
    with open(file_name, 'w', newline='') as csvfile:
        csvfile.write('#')
        datawriter = csv.writer(csvfile, delimiter=',')
        # make sure the order matches
        labels = []
        values = []
        for label, val in file_data.items():
            labels.append(label)
            values.append(np.array(val))
        datawriter.writerow(labels)
        values = np.column_stack([np.array(val) for val in values])
        for row in values:
            datawriter.writerow(row)

def compute_threshold_results(params):
    clas_env, cons_thres = params
    print('\nComputing for threshold {}'.format(cons_thres))
    w_cons, test_stats = clas_env.train_test_classifier(cons_thres)
    return test_stats

def train_test_gini_const(clas_env):
    """ Use gini-index to constrain unfairness of the classifier """

    #NUM_STEPS = 40
    NUM_STEPS = 1
    thresholds = [i / float(NUM_STEPS) for i in range(NUM_STEPS + 1)]

    cons_params = clas_env.model.cons_params
    if cons_params is None:
        thresholds = [None]
        cons_type = 'No'
        proxy_type = 'No'
    else:
        cons_type = cons_params['cons_type']
        proxy_type = cons_params['proxy_type']

    # TODO: merge this with the None cons_params iteration
    compute_threshold_results((clas_env, None))

    inputs = zip(repeat(clas_env), thresholds)
    pool = mp.Pool()
    threshold_test_stats = [compute_threshold_results((clas_env, thres)) for thres in thresholds]
    #threshold_test_stats = pool.map(compute_threshold_results,
    #        inputs)
    pool.close()
    pool.join()

    #threshold_test_stats = [compute_threshold_results(*param_input) for param_input in inputs]

    test_accs = []
    sa_metrics = ['MCR', 'FPR', 'FNR', 'TAR']
    test_sa_values = {sa_metric: defaultdict(list) for sa_metric in sa_metrics}
    test_ginis = {sa_metric: [] for sa_metric in sa_metrics}
    #group_stats = defaultdict(lambda : defaultdict(list))
    group_stats = {sa_metric: defaultdict(list) for sa_metric in sa_metrics}

    for test_stats in threshold_test_stats:
        test_accs.append(test_stats['acc'])
        for metric in test_ginis:
            test_ginis[metric].append(test_stats['gini'][metric])
        for sa_metric in test_sa_values:
            for sa in clas_env.sensitive_attrs:
                test_sa_values[sa_metric][sa].append(test_stats['fr'][sa][sa_metric])
        
        dists = test_stats['dist']
        eval_res = fair_const_eval.construct_eval_res(dists,
                clas_env.y_test, clas_env.x_control_test)
        group_stat = fair_const_eval.evaluate_classification_result(
                eval_res)
        offset = 3
        for group, stats in group_stat.items():
            for sa_metric, sa_metric_gini in zip(sa_metrics,
                    stats[offset:]):
                group_stats[sa_metric][group].append(sa_metric_gini)

    res_dir = 'results/' + clas_env.dataset + '/' + proxy_type + '/' + proxy_type + '_constraint'
    output.create_dir(res_dir)
    output.set_paper_style()

    file_data = {'proxy-thresholds': thresholds}

    def emit_result(label, values):
        #plt.plot(thresholds, values, marker='o', label=label)
        plt.plot(thresholds, values, label=label)
        file_data[label] = values

    label = 'Accuracy'
    emit_result(label, test_accs)
    plot(res_dir, label, 'accuracy', include_legend=False)

    for k, vals in test_ginis.items():
        label = 'Gini (' + k + ')'
        if k == proxy_type or cons_params is None:
            emit_result(label, vals)
    y_label = 'Gini (' + proxy_type + ')'
    plot(res_dir, y_label, 'gini', include_legend=False)

    for sa_metric, sa_metric_values in test_sa_values.items():
        if sa_metric != proxy_type and cons_type is not None:
            continue
        for sa, values in sa_metric_values.items():
            #label = sa_metric + '_' + sa
            #label = sa.strip('race_')
            label = sa[5:] if len(sa) > 5 else sa
            emit_result(label, values)
        plot(res_dir, sa_metric, sa_metric)

    for sa_metric, sa_metric_vals in group_stats.items():
        if sa_metric != proxy_type and cons_params is not None:
            continue
        label = sa_metric + '_group_ginis'
        y_label = 'Group Ginis (' + sa_metric + ')'
        for group, group_vals in sa_metric_vals.items():
            emit_result('Gini_' + sa_metric + '_' + (group[5:] if len(group) > 5 else group),
                    group_vals)
        plot(res_dir, y_label, label)

    save_result(res_dir + '/intergroup_' + clas_env.dataset + '_' + proxy_type + '.csv', file_data)

def get_clas_envs(dataset):
    ds_proxies = {'ProPublica': ['FPR', 'FNR'],
            'Adult': ['TAR']}
    proxies = ds_proxies[dataset]

    clas_env = ClassifierEnv(None)
    clas_env.load_data(dataset)
    #yield clas_env

    cons_type = 'abs-gini'
    for proxy_type in proxies:
        cons_params = {'cons_type': cons_type,
                        'proxy_type': proxy_type}
        yield clas_env.set_cons_params(cons_params)

def evaluate_constraints():
    #dataset = 'Adult'
    dataset = 'ProPublica'

    #train_test_unconst(clas_env)
    #train_test_classic_fairness_const(clas_env)

    for clas_env in get_clas_envs(dataset):
        train_test_gini_const(clas_env)

def main():
    evaluate_constraints()


if __name__ == '__main__':
    main()
