
import sys
import traceback
import numpy as np
from random import seed, shuffle
import cvxpy as cvx
import dccp
from dccp.problem import is_dccp
import utils as ut
from sklearn.base import BaseEstimator
import pickle

# remove
import sys
sys.path.insert(0, '../opt_sol/')
import funcs_disp_mist as fdm
from copy import deepcopy
###

SEED = 1122334455
seed(SEED) # set the random seed so that the random permutations can be reproduced again
np.random.seed(SEED)

class FairnessConstrainedClassifier(BaseEstimator):

    def __init__(self, opt_params={}, obj_params=None):
            #cons_params=None):
        """
        opt_params = {tau, mu, eps}
        Parameters for the optimization algorithm.

        obj_params = {loss_func in {'logreg'}}
        Parameters for the objective function.

        cons_params = {cons_type in {'abs_gini', 'cov'},
                        proxy_type in {'FPR', 'FNR', 'TAR'},
                        sens_attrs_cov_thres}
        Parameters for the constraint.
        """
        self.opt_params = opt_params
        self.obj_params = obj_params
        self.constraints = {}
        #self.cons_params = cons_params
        #self.cons_strength = None
        #self.ref_cons_val = None

    #def set_cons_strength(self, strength):
    #    self.cons_strength = strength

    def add_constraint(self, cons_name, cons_params):
        assert not cons_name in self.constraints

        self.cons_name = cons_name

        self.constraints[cons_name] = cons_params

    def set_cons_strength(self, cons_name, cons_strength):
        self.constraints[cons_name]['cons_strength'] = cons_strength

    def clear_constraints(self):
        self.constraints.clear()

    #def train_model(x, y, x_control, loss_function="logreg", EPS=1e-6, cons_params=None):
    def fit(self, x, y, x_control, seed=None):

        assert len(self.constraints) == 1

        loss_function = "logreg" # perform the experiments with logistic regression
        EPS = 1e-4

        #print("x_control:", x_control)
        for k in list(x_control.keys()):
                if k!= "race_Caucasian":
                        del x_control[k]
        sensitive_attrs = list(x_control.keys())

        def train_test_classifier(cons_params):
                w = fdm.train_model_disp_mist(x, y, x_control, loss_function, EPS, cons_params)

                train_score, _, cov_all_train, _, s_attr_to_fp_fn_train, _ = fdm.get_clf_stats(w, x, y, x_control, x, y, x_control, sensitive_attrs)

                # accuracy and FPR are for the test because we need of for plotting
                # the covariance is for train, because we need it for setting the thresholds
                return w, cov_all_train
               
        cons_strength = self.constraints[self.cons_name].get('cons_strength')
        if cons_strength is None:
            cons_params = None # constraint parameters, will use them later
        else:
            proxy_type = self.constraints[self.cons_name]['proxy_type']
            if proxy_type == 'FPR':
                cons_type = 1
            elif proxy_type == 'FNR':
                cons_type = 2
            else:
                print("wrong cons name:", self.cons_name)
                exit(1)

            sensitive_attrs_to_cov_thresh = deepcopy(self.cov_all_train_uncons)
            for s_attr in list(sensitive_attrs_to_cov_thresh.keys()):
                for cov_type in list(sensitive_attrs_to_cov_thresh[s_attr].keys()):
                    for s_val in sensitive_attrs_to_cov_thresh[s_attr][cov_type]:
                        sensitive_attrs_to_cov_thresh[s_attr][cov_type][s_val] *= cons_strength

            tau = 5.0
            mu = 1.2
            cons_params = {"cons_type": cons_type, 
                                            "tau": tau, 
                                            "mu": mu, 
                                            "sensitive_attrs_to_cov_thresh": sensitive_attrs_to_cov_thresh}


        w_uncons, cov_all_train_uncons = train_test_classifier(cons_params)

        if cons_strength is None:
            self.cov_all_train_uncons = cov_all_train_uncons
        self.w = np.array(w_uncons).flatten()
        return

        # lookup version

        # TODO: fix this
        assert len(self.constraints) == 1
        cons_strength = self.constraints[self.cons_name].get('cons_strength')
        if cons_strength is not None:
            assert(seed in range(10))
            proxy_type = self.constraints[self.cons_name]['proxy_type']
            with open('../opt_sol/weights.pkl', 'rb') as weights_file:
                threshold_weights = pickle.load(weights_file)
                self.w = np.array(threshold_weights[seed]['weights'][proxy_type][cons_strength]).flatten()
            #self.w = np.array(self.w.value).flatten()
            #print('loaded weights:', self.w)
            return

        # self-computed version

        # cons_type, sens_attrs_cov_thres, take_initial_sol, gamma, tau, mu, EPS, cons_type
        """

        Function that trains the model subject to various fairness constraints.
        If no constraints are given, then simply trains an unaltered classifier.
        Example usage in: "disparate_mistreatment/synthetic_data_demo/decision_boundary_demo.py"

        ----

        Inputs:

        X: (n) x (d+1) numpy array -- n = number of examples, d = number of features, one feature is the intercept
        y: 1-d numpy array (n entries)
        x_control: dictionary of the type {"s": [...]}, key "s" is the sensitive feature name, and the value is a 1-d list with n elements holding the sensitive feature values
        loss_function: the loss function that we want to optimize -- for now we have implementation of logistic loss, but other functions like hinge loss can also be added
        EPS: stopping criteria for the convex solver. check the CVXPY documentation for details. default for CVXPY is 1e-6

        cons_params: is None when we do not want to apply any constraints
        otherwise: cons_params is a dict with keys as follows:
            - cons_type: 
                - 0 for all misclassifications 
                - 1 for FPR
                - 2 for FNR
                - 4 for both FPR and FNR
                - 5 for FPR gini constraint
                - 6 for FNR gini constraint
            - tau: DCCP parameter, controls how much weight to put on the constraints, if the constraints are not satisfied, then increase tau -- default is DCCP val 0.005
            - mu: DCCP parameter, controls the multiplicative factor by which the tau increases in each DCCP iteration -- default is the DCCP val 1.2
            - take_initial_sol: whether the starting point for DCCP should be the solution for the original (unconstrained) classifier -- default value is True
            - sens_attrs_cov_thres: covariance threshold for each cons_type, eg, key 1 contains the FPR covariance
        ----

        Outputs:

        w: the learned weight vector for the classifier

        """

        max_iters = 100 # for the convex program
        max_iter_dccp = 50  # for the dccp algo
        
        num_points, num_features = x.shape
        self.w = cvx.Variable(num_features) # this is the weight vector

        # initialize a random value of w
        np.random.seed(112233)
        self.w.value = np.random.rand(x.shape[1])

        #loss = self.get_loss(x, y, x_control)
        loss = cvx.sum_entries( cvx.logistic( cvx.mul_elemwise(-y, x*self.w))) / len(y)

        constraints = []
        constraint_terms = {}
        for cons_name, cons_params in self.constraints.items():
            #print("executing for cons params:", cons_params)
            cons_constraints, constraint_term = \
                    self.get_constraints(
                    x, y, x_control, cons_params)
            print("Adding constraints:", cons_constraints, "\n")
            constraints.extend(cons_constraints)
            if constraint_term is not None:
                constraint_terms[cons_name] = constraint_term#(constraint_term, cons_params['sens_feature'])

        assert len(constraints) == 0 and len(constraint_terms) == len(self.constraints) \
                or len(constraint_terms) == 0

        # construct the cvxpy problem
        prob = cvx.Problem(cvx.Minimize(loss), constraints)

        if prob.is_dcp():
            #print("Problem is DCP (disciplined convex program)")
            pass
        elif is_dccp(prob):
            #print("Problem is DCCP (disciplined convex-concave program)")
            pass
        else:
            print("Problem is neigher DCP nor DCCP")
            sys.exit(1)

        # initialize a random value of w
        np.random.seed(SEED)
        self.w.value = np.random.rand(x.shape[1])

        try:
            # default dccp parameters, need to be varied per dataset
            #tau = self.opt_params.get('tau', 0.005)
            tau = self.opt_params.get('tau', 2.0) # Compas 1-5, Adult 2
            mu = self.opt_params.get('mu', 1.2)
            eps = self.opt_params.get('eps', 1e-6)

            prob.solve(method='dccp', tau=tau, mu=mu, tau_max=1e10,
                solver=cvx.ECOS, verbose=False, feastol=eps, abstol=eps,
                reltol=eps, feastol_inacc=eps, abstol_inacc=eps,
                reltol_inacc=eps, max_iters=max_iters,
                max_iter=max_iter_dccp)
            
            assert(prob.status == 'Converged' or prob.status == 'optimal'), "problem status is {}".format(prob.status)
            #print("Optimization done, problem status:", prob.status)

        except:
            traceback.print_exc()
            sys.stdout.flush()
            sys.exit(1)


        # check that the fairness constraint is satisfied
        for f_c in constraints:
            assert(f_c.value == True)

        for cons_name, cons_term in constraint_terms.items():
            #cons_term_value = cons_term(self.w.value, x, y, x_control[sens_feature])
            cons_term_value = cons_term.value
            print("setting cons_term value for constraint {} to value {}".format(cons_name, cons_term_value))#cons_term.value))
            self.constraints[cons_name]['ref_cons_val'] = cons_term_value#cons_term.value

        self.w = np.array(self.w.value).flatten()
        # flatten converts it to a 1d array

        print("computed weights:", self.w)

    def dist_to_boundary(self, X):
        return np.dot(X, self.w)

    def predict_proba(self, X):
        dist_boundary = self.dist_to_boundary(X)
        probas = 1 / (1 + np.exp(-dist_boundary))
        return probas

    def predict(self, X):
        dist_boundary = self.dist_to_boundary(X)
        return (dist_boundary > 0).astype(float)

    def get_loss(self, x, y, x_control):
        loss_function = 'logreg' if self.obj_params is None \
            else self.obj_params['loss_func']

        if loss_function == 'logreg':
            # constructing the logistic loss problem
            loss = sum_entries( logistic( mul_elemwise(-y,
                x * self.w) ) ) / x.shape[0]
            # we are converting y to a diagonal matrix for consistency
        else:
            raise ValueError("Unsupported loss function {}".format(loss_function))

        return loss


    def get_constraints(self, x, y, x_control, cons_params):
        if cons_params is None:
            # just train a simple classifier, no fairness constraints
            return [], None

        cons_type = cons_params['cons_type']
        #if cons_type == 'abs-gini':
        #    # TODO: refactor to match ref_cons_val etc. handling of cov constraint
        #    assert cons_params['ref_cons_val'] is not None

        #    constraint_term = self.get_abs_gini_constraint(x, y,
        #            x_control, cons_params['proxy_type'])
        #    if cons_params['cons_strength'] is None:
        #        constraints = []
        #    else:
        #        constraints = [constraint_term <= 
        #                cons_params['cons_strength'] * \
        #                cons_params['ref_cons_val']]

        #elif cons_type == 'cov':
        if cons_type == 'cov':
            cov_thres = cons_params.get('cons_strength')
            if cov_thres is not None:
                ref_cons_val = cons_params['ref_cons_val']
                assert ref_cons_val is not None
                constraint_term = None
                cov_thres *= ref_cons_val

            constraints, constraint_term = self.get_constraints_cov(
                    x, y, x_control,
                    cons_params['proxy_type'],
                    cons_params['sens_feature'],
                    cov_thres)

        else:
            raise ValueError("Unsupported constraint type '{}'".format(cons_type))

        return constraints, constraint_term

    def get_group_stats(self, x_train, y_train, x_control_train):
        """
        proxy types: FPR, FNR, TAR (total acceptance rate)
        """

        # partition the training set into sensitive groups
        sens_groups = x_control_train.values() if len(x_control_train) > 1 else \
                [x_control_train.itervalues().next(), \
                np.logical_not(x_control_train.itervalues().next())]
        for i in range(len(y_train)):
            assert sum(sg[i] for sg in sens_groups) == 1

        proxy_type = self.cons_params['proxy_type']
        assert proxy_type in ['FPR', 'FNR', 'TAR']
        pos_neg_filter = y_train if proxy_type == 'FNR' else (-y_train if proxy_type == 'FPR' else np.ones(len(y_train)))
        pos_neg_filter = (pos_neg_filter + 1.) / 2. # convert to {0,1}

        # partition users by sensitive feature and by groud-truth label
        interest_groups = [x_train[np.logical_and(sg.astype(bool),
            pos_neg_filter.astype(bool))] for sg in sens_groups]

        # computes the proxies for the FPR or FNR as sum(f(x)) / size(group)
        group_stats = [sum_entries(ig * self.w)/len(ig) for ig in \
                interest_groups if len(ig) > 0]

        return group_stats

    def get_gini_constraint(self, x_train, y_train, x_control_train):
        group_stats = self.get_group_stats(x_train, y_train, x_control_train)

        #modified for one person per group
        #fr_proxies = [ig[0] * w for ig in interest_groups if len(ig) > 0]
        #fr_proxies = [x_train[i] * w for i in range(x_train.shape[0])]
    
        gini_numerator = sum( sum( abs( q1 - q2 ) for q2 in group_stats) for q1 in group_stats)
        # invert sign of denominator if we are constraining for FPR because we want the avg distances from the boundary to be negative
        #denom_fac = -1 if mode == 5 else 1
        #gini_denominator = denom_fac * 2 * len(sens_groups) * sum( abs(fr) for fr in fr_proxies)
        gini_denominator = 2 * len(group_stats) * sum( abs(q) for q \
                in group_stats)
        #gini_denominator = 2 * len(sens_groups) * abs(sum( fr for fr in fr_proxies))

        #gini_constraint = gini_numerator <= gini_thres * gini_denominator
        #fr_proxy_constraints = [fr_proxy <= 0 for fr_proxy in fr_proxies]
        #denom_constraint = gini_denominator > 0
        #return [gini_constraint, denom_constraint], fr_proxies, gini_numerator, gini_denominator
        return gini_numerator, gini_denominator

    def get_abs_gini_constraint(self, x_train, y_train,
            x_control_train):
        group_stats = self.get_group_stats(x_train, y_train,
                x_control_train)
        deprivations_sum = sum( sum( max_elemwise(0, q2 - q1) for q2 \
                in group_stats) for q1 in group_stats)
        constraint_term = deprivations_sum / np.square(
                len(group_stats))
        return constraint_term

    def get_constraints_cov(self, x_train, y_train,
            x_control_train, proxy_type, sens_feature,
            cons_thres):

        print("using constraint value:", cons_thres)

        # TODO: fix this
        #if False and cons_thres == 0:
        #    if proxy_type == 'OMR':
        #        conv_cons_type = 0
        #    elif proxy_type == 'FPR':
        #        conv_cons_type = 1
        #    elif proxy_type == 'FNR':
        #        conv_cons_type = 2

        #    sensitive_attrs_to_cov_thresh = {sens_feature: {0: {0:0, 1:0}, 1: {0:0, 1:0}, 2:{0:0, 1:0}}}#cov_thres, 1:0}}}
        #    constraints = get_constraint_list_cov(x_train, y_train, x_control_train,
        #        sensitive_attrs_to_cov_thresh, conv_cons_type, self.w)
        #    #return constraints, cons_sum_dict
        #    return constraints

        """
        get the list of constraints to be fed to the minimizer

        #cons_type == 0: means the whole combined misclassification constraint (without FNR or FPR)
        #cons_type == 1: FPR constraint
        #cons_type == 2: FNR constraint
        #cons_type == 4: both FPR as well as FNR constraints

        sens_attrs_cov_thres: is a dict like {s: {cov_type: val}}
        s is the sensitive attr
        cov_type is the covariance type. contains the covariance for all misclassifications, FPR and for FNR etc
        """

        assert proxy_type in ['OMR', 'FPR', 'FNR'], "proxy type '{}' is not one of 'OMR', 'FPR', 'FNR'".format(proxy_type)

        attr_arr = x_control_train[sens_feature]
        # TODO: do we need this?
        attr_arr_transformed, index_dict = ut.get_one_hot_encoding(attr_arr)
        #        
        if index_dict is None: # binary attribute, in this case, the attr_arr_transformed is the same as the attr_arr

            s_val_to_total = {}
            s_val_to_avg = {}
            cons_sums = {}

            if proxy_type == 'FPR':
                proxy_filter = y_train == -1
            elif proxy_type == 'FNR':
                proxy_filter = y_train == +1
            else:
                proxy_filter = np.ones(len(y_train), dtype=bool)

            for v in set(attr_arr):
                s_val_to_total[v] = sum((x_control_train[sens_feature] == v) & proxy_filter)

            
            s_val_to_avg[0] = s_val_to_total[1] / float(s_val_to_total[0] + s_val_to_total[1]) # A_0 in our formulation
            s_val_to_avg[1] = 1.0 - s_val_to_avg[0]

            for v in set(attr_arr):
                idx = (x_control_train[sens_feature] == v) & proxy_filter

                dist_bound_prod = cvx.mul_elemwise(y_train[idx], x_train[idx] * self.w) # y.f(x)
               
                cons_sums[v] = cvx.sum_entries( cvx.min_elemwise(0, dist_bound_prod) ) * (s_val_to_avg[v] )

        if cons_thres is None:
            return [], cvx.abs(cons_sums[0] - cons_sums[1])
        else:
            #if cons_thres == 0:
            #    cons_thres = .2
            #DCCP constraints
            constraints = [cons_sums[1] <= cons_sums[0] + cons_thres,
                cons_sums[1] >= cons_sums[0] - cons_thres]
            return constraints, None

        #    
        #else: # otherwise, its a categorical attribute, so we need to set the cov thresh for each value separately
        #    # need to fill up this part
        #    raise Exception("Fill the constraint code for categorical sensitive features... Exiting...")
        #    sys.exit(1)

def get_constraint_list_cov(x_train, y_train, x_control_train, sensitive_attrs_to_cov_thresh, cons_type, w):

    """
    get the list of constraints to be fed to the minimizer

    cons_type == 0: means the whole combined misclassification constraint (without FNR or FPR)
    cons_type == 1: FPR constraint
    cons_type == 2: FNR constraint
    cons_type == 4: both FPR as well as FNR constraints

    sensitive_attrs_to_cov_thresh: is a dict like {s: {cov_type: val}}
    s is the sensitive attr
    cov_type is the covariance type. contains the covariance for all misclassifications, FPR and for FNR etc
    """

    constraints = []
    for attr in list(sensitive_attrs_to_cov_thresh.keys()):

        attr_arr = x_control_train[attr]
        attr_arr_transformed, index_dict = ut.get_one_hot_encoding(attr_arr)
                
        if index_dict is None: # binary attribute, in this case, the attr_arr_transformed is the same as the attr_arr

            s_val_to_total = {ct:{} for ct in [0,1,2]} # constrain type -> sens_attr_val -> total number
            s_val_to_avg = {ct:{} for ct in [0,1,2]}
            cons_sum_dict = {ct:{} for ct in [0,1,2]} # sum of entities (females and males) in constraints are stored here

            for v in set(attr_arr):
                s_val_to_total[0][v] = sum(x_control_train[attr] == v)
                s_val_to_total[1][v] = sum(np.logical_and(x_control_train[attr] == v, y_train == -1)) # FPR constraint so we only consider the ground truth negative dataset for computing the covariance
                s_val_to_total[2][v] = sum(np.logical_and(x_control_train[attr] == v, y_train == +1))


            for ct in [0,1,2]:
                s_val_to_avg[ct][0] = s_val_to_total[ct][1] / float(s_val_to_total[ct][0] + s_val_to_total[ct][1]) # N1/N in our formulation, differs from one constraint type to another
                s_val_to_avg[ct][1] = 1.0 - s_val_to_avg[ct][0] # N0/N

            
            for v in set(attr_arr):

                idx = x_control_train[attr] == v                


                #################################################################
                # #DCCP constraints
                dist_bound_prod = mul_elemwise(y_train[idx], x_train[idx] * w) # y.f(x)
                
                cons_sum_dict[0][v] = sum_entries( min_elemwise(0, dist_bound_prod) ) * (s_val_to_avg[0][v] / len(x_train)) # avg misclassification distance from boundary
                cons_sum_dict[1][v] = sum_entries( min_elemwise(0, mul_elemwise( (1 - y_train[idx])/2.0, dist_bound_prod) ) ) * (s_val_to_avg[1][v] / sum(y_train == -1)) # avg false positive distance from boundary (only operates on the ground truth neg dataset)
                cons_sum_dict[2][v] = sum_entries( min_elemwise(0, mul_elemwise( (1 + y_train[idx])/2.0, dist_bound_prod) ) ) * (s_val_to_avg[2][v] / sum(y_train == +1)) # avg false negative distance from boundary
                #################################################################

                
            if cons_type == 4:
                cts = [1,2]
            elif cons_type in [0,1,2]:
                cts = [cons_type]
            
            else:
                raise Exception("Invalid constraint type")


            #################################################################
            #DCCP constraints
            for ct in cts:
                thresh = abs(sensitive_attrs_to_cov_thresh[attr][ct][1] - sensitive_attrs_to_cov_thresh[attr][ct][0])
                print("using thres:", thresh)
                constraints.append( cons_sum_dict[ct][1] <= cons_sum_dict[ct][0]  + thresh )
                constraints.append( cons_sum_dict[ct][1] >= cons_sum_dict[ct][0]  - thresh )

            #################################################################


            
        else: # otherwise, its a categorical attribute, so we need to set the cov thresh for each value separately
            # need to fill up this part
            raise Exception("Fill the constraint code for categorical sensitive features... Exiting...")
            sys.exit(1)
            

    return constraints, None


#def get_sensitive_attr_constraint_fpr_fnr_cov(model, x_arr, y_arr_true, y_arr_dist_boundary, x_control_arr, verbose=False):
def get_sensitive_attr_constraint_fpr_fnr_cov(model, x_arr, y_arr_true, x_control_arr, verbose=False):
    
    """
    Here we compute the covariance between sensitive attr val and ONLY misclassification distances from boundary for False-positives
    (-N_1 / N) sum_0(min(0, y.f(x))) + (N_0 / N) sum_1(min(0, y.f(x))) for all misclassifications
    (-N_1 / N) sum_0(min(0, (1-y)/2 . y.f(x))) + (N_0 / N) sum_1(min(0,  (1-y)/2. y.f(x))) for FPR
    y_arr_true are the true class labels
    y_arr_dist_boundary are the predicted distances from the decision boundary
    If the model is None, we assume that the y_arr_dist_boundary contains the distace from the decision boundary
    If the model is not None, we just compute a dot product or model and x_arr
    for the case of SVM, we pass the distace from bounday becase the intercept in internalized for the class
    and we have compute the distance using the project function
    this function will return -1 if the constraint specified by thresh parameter is not satifsified
    otherwise it will reutrn +1
    if the return value is >=0, then the constraint is satisfied
    """

        
    assert(x_arr.shape[0] == x_control_arr.shape[0])
    if len(x_control_arr.shape) > 1: # make sure we just have one column in the array
        assert(x_control_arr.shape[1] == 1)
    if len(set(x_control_arr)) != 2: # non binary attr
        raise Exception("Non binary attr, fix to handle non bin attrs")

    
    arr = []
    if model is None:
        arr = y_arr_dist_boundary * y_arr_true # simply the output labels
    else:
        arr = np.dot(x_arr, model) * y_arr_true # the product with the weight vector -- the sign of this is the output label
    arr = np.array(arr)

    s_val_to_total = {ct:{} for ct in [0,1,2]}
    s_val_to_avg = {ct:{} for ct in [0,1,2]}
    cons_sum_dict = {ct:{} for ct in [0,1,2]} # sum of entities (females and males) in constraints are stored here

    for v in set(x_control_arr):
        s_val_to_total[0][v] = sum(x_control_arr == v)
        s_val_to_total[1][v] = sum(np.logical_and(x_control_arr == v, y_arr_true == -1))
        s_val_to_total[2][v] = sum(np.logical_and(x_control_arr == v, y_arr_true == +1))


    for ct in [0,1,2]:
        s_val_to_avg[ct][0] = s_val_to_total[ct][1] / float(s_val_to_total[ct][0] + s_val_to_total[ct][1]) # N1 / N
        s_val_to_avg[ct][1] = 1.0 - s_val_to_avg[ct][0] # N0 / N

    
    for v in set(x_control_arr):
        idx = x_control_arr == v
        dist_bound_prod = arr[idx]

        cons_sum_dict[0][v] = sum( np.minimum(0, dist_bound_prod) ) * (s_val_to_avg[0][v] / len(x_arr))
        cons_sum_dict[1][v] = sum( np.minimum(0, np.dot(( (1 - y_arr_true[idx]) / 2 ), dist_bound_prod)) ) * (s_val_to_avg[1][v] / sum(y_arr_true == -1))
        cons_sum_dict[2][v] = sum( np.minimum(0, np.dot(( (1 + y_arr_true[idx]) / 2 ), dist_bound_prod)) ) * (s_val_to_avg[2][v] / sum(y_arr_true == +1))
        

    cons_type_to_name = {0:"ALL", 1:"FPR", 2:"FNR"}
    for cons_type in [0,1,2]:
        cov_type_name = cons_type_to_name[cons_type]    
        cov = cons_sum_dict[cons_type][1] - cons_sum_dict[cons_type][0]
        if verbose == True:
            print("Covariance for type '{0}' is: {1:.7f}".format(cov_type_name, cov))
        
    #return cons_sum_dict
    return cons_sum_dict[1][1] - cons_sum_dict[1][0]
