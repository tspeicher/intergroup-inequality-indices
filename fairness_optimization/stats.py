import numpy as np
from copy import deepcopy
import utils as ut

def get_clf_stats(w, x_train, y_train, x_control_train, x_test, y_test, x_control_test, sensitive_attrs):

    # TODO: is it save to ignore this? We don't have separate weight vectors per group
    
    #assert(len(sensitive_attrs) == 1) # ensure that we have just one sensitive attribute
    #s_attr = sensitive_attrs[0] # for now, lets compute the accuracy for just one sensitive attr

    #for s_attr in sensitive_attrs:

    # compute distance from boundary
    distances_boundary_train = get_distance_boundary(w, x_train)
    distances_boundary_test = get_distance_boundary(w, x_test)

    # compute the class labels
    all_class_labels_assigned_train = np.sign(distances_boundary_train)
    all_class_labels_assigned_test = np.sign(distances_boundary_test)

    train_score, test_score, correct_answers_train, correct_answers_test = ut.check_accuracy(None, x_train, y_train, x_test, y_test, all_class_labels_assigned_train, all_class_labels_assigned_test)

    cov_all_train = {}
    cov_all_test = {}

    train_stats = {'acc': train_score}
    test_stats = {'acc': test_score}

    # to here

    print_stats = False # we arent printing the stats for the train set to avoid clutter

    # uncomment these lines to print stats for the train fold
    # print "*** Train ***"
    # print "Accuracy: %0.3f" % (train_score)
    # print_stats = True
    s_attr_to_fp_fn_train = get_fpr_fnr_sensitive_features(y_train, all_class_labels_assigned_train, x_control_train, sensitive_attrs, print_stats)
    train_stats['fr'] = s_attr_to_fp_fn_train
    gini_train = get_gini_coefficient(s_attr_to_fp_fn_train, ['MCR', 'FPR', 'FNR', 'TAR'])
    train_stats['gini'] = gini_train
    train_stats['dist'] = distances_boundary_train

    print("\nAccuracy: %0.3f" % (test_score))
    print_stats = True # only print stats for the test fold
    s_attr_to_fp_fn_test = get_fpr_fnr_sensitive_features(y_test, all_class_labels_assigned_test, x_control_test, sensitive_attrs, print_stats)
    test_stats['fr'] = s_attr_to_fp_fn_test
    gini_test = get_gini_coefficient(s_attr_to_fp_fn_test, ['MCR', 'FPR', 'FNR', 'TAR'])
    test_stats['gini'] = gini_test
    test_stats['dist'] = distances_boundary_test

    #return train_score, test_score, cov_all_train, cov_all_test, s_attr_to_fp_fn_train, s_attr_to_fp_fn_test
    #return train_score, test_score, s_attr_to_fp_fn_train, s_attr_to_fp_fn_test
    return train_stats, test_stats

def get_distance_boundary(w, x):

    """
        if we have boundaries per group, then use those separate boundaries for each sensitive group
        else, use the same weight vector for everything
    """

    distances_boundary = np.dot(x, w)
    return distances_boundary


def get_fpr_fnr_sensitive_features(y_true, y_pred, x_control, sensitive_attrs, verbose = False):

    # we will make some changes to x_control in this function, so make a copy in order to preserve the origianl referenced object
    x_control_internal = deepcopy(x_control)

    s_attr_to_fp_fn = {}

    if verbose == True:
        print("||  s  || FPR. || FNR. ||")
    
    for s in sensitive_attrs:
# convert from {0, 1} to {-1, 1}
        s_attr_vals = x_control_internal[s].astype(bool)
        #s_attr_vals = 2. * x_control_internal[s] - 1

        s_attr_to_fp_fn[s] = {}
        y_true_local = y_true[s_attr_vals]
        y_pred_local = y_pred[s_attr_vals]
        
        acc = float(sum(y_true_local==y_pred_local)) / len(y_true_local)

        fp = sum(np.logical_and(y_true_local == -1.0, y_pred_local == +1.0)) # something which is -ve but is misclassified as +ve
        fn = sum(np.logical_and(y_true_local == +1.0, y_pred_local == -1.0)) # something which is +ve but is misclassified as -ve
        tp = sum(np.logical_and(y_true_local == +1.0, y_pred_local == +1.0)) # something which is +ve AND is correctly classified as +ve
        tn = sum(np.logical_and(y_true_local == -1.0, y_pred_local == -1.0)) # something which is -ve AND is correctly classified as -ve

        all_neg = sum(y_true_local == -1.0)
        all_pos = sum(y_true_local == +1.0)

        fpr = float(fp) / float(fp + tn)
        fnr = float(fn) / float(fn + tp)
        tpr = float(tp) / float(tp + fn)
        tnr = float(tn) / float(tn + fp)

        tar = float(fp + tp) / len(y_true_local)

        s_attr_to_fp_fn[s]["FP"] = fp
        s_attr_to_fp_fn[s]["FN"] = fn
        s_attr_to_fp_fn[s]["FPR"] = fpr
        s_attr_to_fp_fn[s]["FNR"] = fnr
        s_attr_to_fp_fn[s]["TAR"] = tar
        
        acc = (tp + tn) / (tp + tn + fp + fn)
        s_attr_to_fp_fn[s]["ACC"] = acc
        s_attr_to_fp_fn[s]["MCR"] = 1 - acc
        if verbose == True:
            #if isinstance(s_val, float): # print the int value of the sensitive attr val
            #    s_val = int(s_val)
            #print("||  %s  || %0.2f || %0.2f ||" % (s_val, fpr, fnr))
            print("||  %s  || %0.2f || %0.2f ||" % (s, fpr, fnr))

        
    return s_attr_to_fp_fn

def get_gini_coefficient(values, targets):
    ginis = {}
    for target in targets:
        orig_target = target
        gini_denominator = np.sum( v[target] for v in values.values() )
        gini_denominator = 2 * len(values) * gini_denominator
        # this does not change under inversion
        gini_numerator = np.sum( np.sum( np.abs(v1[target] - v2[target]) for v2 in values.values() ) for v1 in values.values() )
        #gini_denominator = 2 * len(values) * np.sum( v[target] for v in values.values() )
        ginis[orig_target.upper()] = gini_numerator / gini_denominator
    return ginis

